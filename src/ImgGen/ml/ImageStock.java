package ImgGen.ml;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class ImageStock {
    public enum ImageSize { SMALL, LARGE };
    public enum ImageName { CROSS, YOUTUBE, ARROW, PHONE, FACE };

    public static BitmapImage getImageByID(int imageID, ImageSize imageSize) throws Exception {
        String fileName;

        if (imageSize == ImageSize.SMALL) {
            fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/logos/small/images(" + imageID +").jpg";
        } else {
            fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/logos/images(" + imageID + ").jpg";
        }
        return new BitmapImage(ImageIO.read(new File(fileName)));
    }

    public static BitmapImage getSampleImage(ImageSize imageSize, ImageName imageName) throws IOException {
        String fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/cross-small.jpg";

        if (imageSize == ImageSize.SMALL) {
            if (imageName == ImageName.CROSS)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/cross-small.jpg";

            if (imageName == ImageName.YOUTUBE)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/youtube-small.jpg";

            if (imageName == ImageName.ARROW)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/arrow-small.jpg";

            if (imageName == ImageName.PHONE)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/phone-small.jpg";

            if (imageName == ImageName.FACE)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/face-small.jpg";
        }

        if (imageSize == ImageSize.LARGE) {
            if (imageName == ImageName.CROSS)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/cross-large.jpg";

            if (imageName == ImageName.YOUTUBE)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/youtube-large.jpg";

            if (imageName == ImageName.ARROW)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/arrow-large.jpg";

            if (imageName == ImageName.PHONE)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/phone-large.jpg";

            if (imageName == ImageName.FACE)
                fileName = "/Users/sanjav/Documents/workspace-dip/dip/images/1/face-large.jpg";
        }

        return new BitmapImage(ImageIO.read(new File(fileName)));
    }
}
