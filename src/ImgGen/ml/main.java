package ImgGen.ml;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;

public class main extends JPanel {
    BitmapImage image;
    public main() {
        try {
            LearningActivity activity = new LearningActivity();
            this.image = activity.getConstructedImage();
            setBackground(Color.white);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void paint(Graphics g) {
        try {
            Graphics2D g2D;
            g2D = (Graphics2D) g;
            g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

            g2D.drawImage(image.getBufferedImage(), new AffineTransform(), this);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public static void main(String[] args) throws Exception {
        JFrame frame = new JFrame("2D Images ");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.getContentPane().add("Center", new main());
        frame.pack();
        frame.setSize(new Dimension(1000, 1000));
        frame.setVisible(true);
    }
}
