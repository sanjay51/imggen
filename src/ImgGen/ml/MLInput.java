package ImgGen.ml;


import org.encog.neural.data.NeuralDataSet;
import org.encog.neural.data.basic.BasicNeuralDataSet;

import java.util.ArrayList;
import java.util.List;

public class MLInput {
    List<List<Integer>> inputs;
    List<List<Integer>> outputs;

    public MLInput() {
        this.inputs = new ArrayList<>();
        this.outputs = new ArrayList<>();
    }

    public void add(List<Integer> input, List<Integer> output) {
        this.inputs.add(input);
        this.outputs.add(output);
    }

    public void add(ImageStock.ImageName imageName) throws Exception {
        BitmapImage inputImage = ImageStock.getSampleImage(ImageStock.ImageSize.SMALL, imageName);
        BitmapImage outputImage = ImageStock.getSampleImage(ImageStock.ImageSize.LARGE, imageName);

        this.add(inputImage.getPixels(), outputImage.getPixels());
    }

    public void add(int imageID) throws Exception {
        BitmapImage inputImage = ImageStock.getImageByID(imageID, ImageStock.ImageSize.SMALL);
        BitmapImage outputImage = ImageStock.getImageByID(imageID, ImageStock.ImageSize.LARGE);

        this.add(inputImage.getPixels(), outputImage.getPixels());
    }

    public void print() {
        System.out.println("MLInput: ");
        for(int i = 0; i < inputs.size(); i++) {
            System.out.print(inputs.get(i) + " " + outputs.get(i) + "\n");
        }

        System.out.println("------------");
    }

    public int getOutputLayerSize() {
        return outputs.size();
    }

    public NeuralDataSet getAsNeuralDataSet() {
        double INPUTS[][] = new double[inputs.size()][inputs.get(0).size()];
        for (int i = 0; i < inputs.size(); i++) {
            for (int j = 0; j < inputs.get(i).size(); j++) {
                INPUTS[i][j] = inputs.get(i).get(j) / 255.0;
            }
        }

        double OUTPUT[][] = new double[outputs.size()][outputs.get(0).size()];
        for (int i = 0; i < outputs.size(); i++) {
            for (int j = 0; j < outputs.get(i).size(); j++) {
                OUTPUT[i][j] = outputs.get(i).get(j) / 255.0;
            }
        }

        return new BasicNeuralDataSet(INPUTS, OUTPUT);
    }
}