package ImgGen.ml;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanjav on 1/11/17.
 */
public class BitmapImage {
    BufferedImage bufferedImage;

    public BitmapImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    public BitmapImage getTransformedImage() {
        int newWidth = this.bufferedImage.getWidth() * 5;
        int newHeight = this.bufferedImage.getHeight() * 5;
        BufferedImage newImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < newWidth; x++) {
            for (int y = 0; y < newHeight; y++) {
                Color color = new Color(this.bufferedImage.getRGB(x/5, y/5));
                newImage.setRGB(x, y, color.getRGB());
            }
        }

        return new BitmapImage(newImage);
    }

    public int getPixelValue(int x, int y) {
        Color color = new Color(this.bufferedImage.getRGB(x, y));
        return (color.getRed() + color.getBlue() + color.getGreen())/3;
    }

    public List<Integer> getPixels() {
        List<Integer> pixels = new ArrayList<>();

        for (int x = 0; x < getWidthInPixels(); x++) { //inputImage.getWidthInPixels(); x++) {
            for (int y = 0; y < getHeightInPixels(); y++) { //inputImage.getHeightInPixels(); y++) {
                pixels.add(this.getPixelValue(x,y));
            }
        }

        return pixels;
    }

    public void setPixelValue(int x, int y, int value) {
        Color color = new Color(value, value, value);
        this.bufferedImage.setRGB(x, y, color.getRGB());
    }

    public int getWidthInPixels() {
        return bufferedImage.getWidth();
    }

    public int getHeightInPixels() {
        return bufferedImage.getHeight();
    }

    public BufferedImage getBufferedImage() {
        return this.bufferedImage;
    }
}
