package ImgGen.ml;

import org.encog.ml.data.MLData;

/**
 * Created by sanjav on 1/11/17.
 */
public interface NNClassifier {
    public void train();
    public MLData compute(double[] input);
}
