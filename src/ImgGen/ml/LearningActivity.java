package ImgGen.ml;

/**
 * Created by sanjav on 1/11/17.
 */
public class LearningActivity {

    public BitmapImage getConstructedImage() throws Exception {
        BasicNNClassifier nnClassifier = new BasicNNClassifier(getMLInput());
        nnClassifier.train();
        double[] output = nnClassifier.compute(getSmallImageDataForValidation()).getData();
        BitmapImage constructedImage = getLargeImageFromPixelValues(output);
        return constructedImage.getTransformedImage();
    }

    private MLInput getMLInput() throws Exception {
        MLInput mlInput = new MLInput();
        mlInput.add(ImageStock.ImageName.CROSS);
        mlInput.add(ImageStock.ImageName.PHONE);

        // i begins from one, because we don't have images(0).jpg :)
        for (int i = 1; i <= 50; i++) {
            mlInput.add(i);
        }

        return mlInput;
    }

    private double[] getSmallImageDataForValidation() throws Exception {
        BitmapImage inputImage = ImageStock.getSampleImage(ImageStock.ImageSize.SMALL, ImageStock.ImageName.FACE);
        //inputImage = ImageStock.getImageByID(2, ImageStock.ImageSize.SMALL);
        double INPUT[] = new double[15*15];
        for (int i = 0; i < inputImage.getWidthInPixels(); i++) {
            for (int j = 0; j < inputImage.getHeightInPixels(); j++) {
                int r = inputImage.getPixelValue(i, j);
                INPUT[j + 15*i] = r / 255.0;
            }
        }

        return INPUT;
    }

    private BitmapImage getLargeImageFromPixelValues(double[] pixels) throws Exception {
        BitmapImage image = ImageStock.getSampleImage(ImageStock.ImageSize.LARGE, ImageStock.ImageName.FACE);

        for (int x = 0; x < image.getWidthInPixels(); x++) {
            for (int y = 0; y < image.getHeightInPixels(); y++) {
                //System.out.println(pixels[(x*30) + y]);
                int r = (int) (pixels[(x*30) + y] * 255);
                if (r < 0) r = 0;
                if (r > 255) r = 255;
                 r = image.getPixelValue(x, y);
                image.setPixelValue(x, y, r);
            }
        }

        return image;
    }
}
