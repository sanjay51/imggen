package ImgGen.ml;

import org.encog.engine.network.activation.ActivationFunction;
import org.encog.engine.network.activation.ActivationLOG;
import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.neural.data.NeuralDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.Train;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;

import java.io.File;

public class BasicNNClassifier implements NNClassifier {
    public static final double MAX_ERROR = 0.01;
    private static final String FILE_NAME = "/tmp/neuralnetwork";

    MLInput mlInput;
    BasicNetwork neuralNetwork;
    boolean isTrained;

    public BasicNNClassifier(MLInput mlInput) {
        this.mlInput = mlInput;
    }

    @Override
    public void train() {
        File file = new File(FILE_NAME);
        if (file.exists()) {
            //take a copy of neuralnetwork file first - IMPORTANT !!!!
            this.neuralNetwork = (BasicNetwork) EncogDirectoryPersistence.loadObject(file);
            return;
        }

        NeuralDataSet trainingSet = mlInput.getAsNeuralDataSet();

        //create neural network
        int inputLayerSize = trainingSet.getInputSize();
        int outputLayerSize = trainingSet.getIdealSize();

        int MIDDLE_LAYER_SIZE = 20; // (inputLayerSize + outputLayerSize) / 2;

        neuralNetwork = new BasicNetwork();
        ActivationFunction activationFunction = new ActivationLOG();
        neuralNetwork.addLayer(new BasicLayer(activationFunction, true, inputLayerSize));
        neuralNetwork.addLayer(new BasicLayer(activationFunction, true, MIDDLE_LAYER_SIZE));
        neuralNetwork.addLayer(new BasicLayer(activationFunction, true, MIDDLE_LAYER_SIZE));
        neuralNetwork.addLayer(new BasicLayer(activationFunction, false, outputLayerSize));
        neuralNetwork.getStructure().finalizeStructure();
        neuralNetwork.reset();

        //train
        Train train = new ResilientPropagation(neuralNetwork, trainingSet);
        int count = 1;
        do {
            train.iteration();
            if (count%10 == 0)
            System.out.println("Iteration: " + count + ", error: " + train.getError());
            count++;
        } while (train.getError() > MAX_ERROR);

        EncogDirectoryPersistence.saveObject(file, neuralNetwork);
        System.out.println("Took " + count + " iterations for reducing error to " + MAX_ERROR*100 + "%.");
        isTrained = true;
    }

    @Override
    public MLData compute(double[] input) {
        MLData mlDataInput = new BasicMLData(input);
        return compute(mlDataInput);
    }

    public MLData compute(MLData input) {
        if (!isTrained) {
            train();
        }

        return neuralNetwork.compute(input);
    }
}